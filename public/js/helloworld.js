
document.addEventListener('DOMContentLoaded', ()=>{

// Handling all of our errors here by alerting them
function handleError(error) {
  if (error) {
    alert(error.message);
  }
}
var session = OT.initSession(apiKey, sessionId);
function initializeSession() {



  // Publish if host
  if(host === "true"){
    // HOST START STREAM, PUBLISH FIRST AND IS ON FULL SCREEN
    var publisher = OT.initPublisher('publisher', {
      insertMode: 'append',
      width: '100%',
      height: '100%'
    }, handleError);  
  session.connect(token, function(error) {
    // If the connection is successful, initialize a publisher and publish to the session
    if (error) {
      handleError(error);
    } else {
      session.publish(publisher, handleError);
      session.on('streamCreated', function(event) {
        var subscriberProperties = {insertMode: 'append'};
        var subscriber = session.subscribe(event.stream,
          'subscriberContainer',
          subscriberProperties,
          function (error) {
            if (error) {
              console.log(error);
            } else {
              console.log('Subscriber added.');
            }
        });
      });
    }
    if (session.capabilities.forceDisconnect == 1) {
     var newDiv = document.createElement("div");
      newDiv.innerHTML = "unpublish user";
      newDiv.style = "    z-index: 11111; position: absolute; top: 50px;"
      document.body.insertBefore(newDiv, document.getElementById('chat'));   
      newDiv.addEventListener('click', ()=>{
        console.log('forceUnpusbish user')
      //  session.forceUnpublish(streams[1]);
      })
    } else {
      console.log(session.capabilities, role)
    }
  });
  }else{
    // USER PUBLISH ONLY IF STREAM STARTED IS SHOWN IN SMALL BOX
    session.connect(token, function(error) {
    // If the connection is successful, initialize a publisher and publish to the session
    if (error) {
      handleError(error);
    } else {
      session.once('streamCreated', function(event) {
        
        var publisher = OT.initPublisher('subscriberContainer', {
          insertMode: 'append',
        }, handleError);  
        session.publish(publisher, handleError);
      });
      session.on('streamCreated', function(event) {
        
        var subscriber = session.subscribe(event.stream,
          'publisher',
          {
            insertMode: 'append',
            width: '100%',
            height: '100%'
          },
          function (error) {
            if (error) {
              console.log(error);
            } else {
              console.log('Subscriber added.');
            }
        });
      });
  }
  });
  }
  

}


session.on("signal", function(event) {
  console.log("Signal sent " + event.data);
});

document.getElementById("chat").addEventListener('submit',(event)=>{
  event.preventDefault();
  session.signal(
    {
      data: document.getElementById("chat_text").value
    },
    function(error) {
      if (error) {
        console.log("signal error ("
                     + error.name
                     + "): " + error.message);
      } else {
        console.log('signal was sent');
      }
    }
  );
})
initializeSession()

})