require 'sinatra/base'
require 'opentok'
require 'pry-remote'

raise "You must define API_KEY and API_SECRET environment variables" unless ENV.has_key?("API_KEY") && ENV.has_key?("API_SECRET")

class HelloWorld < Sinatra::Base
  enable :sessions

  set :api_key, ENV['API_KEY']
  set :opentok, OpenTok::OpenTok.new(api_key, ENV['API_SECRET'])
  set :session, opentok.create_session

  get '/' do

    api_key = settings.api_key
    session_id = settings.session.session_id
    if session[:role].nil?
      redirect to('/login')
    else
      token = settings.opentok.generate_token(session_id, :role => session[:role].to_sym)
    end

    erb :index, :locals => {
      :api_key => api_key,
      :session_id => session_id,
      :token => token,
      :role => session[:role],
      :host => session[:host]
    }
  end

  get '/login' do
    erb :login
  end


## PASSWORDS email and kept in webinar model simillary to rooms webinar has many users, user has password, email, role , name to display?
##, eq used here
## if user can stream then his role is poblisher else role is subscriber and host bool is not needed
users = [{email:"user@mail.com", password:"user_password", role:"publisher", host:false}, {email:"admin@mail.com", password:"admin_password", role:"publisher", host:true}, {email:"moderator@mail.com", password:"moderator_password", role:"moderator",  host:true}]


  post '/login' do
    users.each do |user| 
      if (params[:email] == user[:email] && params[:password] == user[:password])
        session[:role] =  user[:role]
        session[:host] = user[:host]
        redirect to('/')
      end
    end
  end

  # start the server if ruby file executed directly
  run! if app_file == $0
end
